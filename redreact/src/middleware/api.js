import axios from "axios"
import { API, API_START, API_END, API_ERROR, ACCESS_DENIED } from "../actions/commonActions"
import { accessDenied, apiError, apiStart, apiEnd } from "../actions/commonActions"

const apiMiddleware = ({dispatch}) => next => action => {

    next(action);

    if (action.type !== API) return;


    /*Divide login and register */

    const {
        url,
        method,
        data,
        accessToken,
        onSuccess,
        onFailure,
        label,
        headers,
        type
    } = action.payload;

  

    const dataOrParams = ["GET", "POST", "DELETE"].includes(method) ? "params" : "data"

    axios.defaults.baseURL = process.env.REACT_APP_BASE_URL || ""
    axios.defaults.headers.common["Content-Type"] = "application/json"
    axios.defaults.headers.common["Authorization"] = `Bearer ${accessToken}`

    if (label) {
        let enhanced_api_start = enhanceActionAPI(API_START, type)
        dispatch(apiStart(enhanced_api_start, label))
    }

    console.log(url);
    console.log(method);
    console.log(headers);
    console.log(data);
    
    axios
        .request({
            url,
            method,
            headers,
            [dataOrParams] : data
        })
        .then(({data}) => {
            console.log(data);
            
            dispatch(onSuccess(data))
        })
        .catch(error => {
            console.log(error);
            
            let enhanced_error_type = enhanceActionAPI(API_ERROR, type)
            dispatch(apiError(enhanced_error_type, error))
            dispatch(onFailure(enhanced_error_type, error))

            if (error.response && error.response.status === 403) {
                let enhanced_deniend_type = enhanceActionAPI(ACCESS_DENIED, type)
                dispatch(accessDenied(enhanced_deniend_type, window.location.pathname))
            }
        })
        .finally(() => {
            if (label) {
                let enhanced_end_type = enhanceActionAPI(API_END, type)
                dispatch(apiEnd(enhanced_end_type, label))
            }
        })
}

const enhanceActionAPI = (ActionAPI, type ) => {
    return ActionAPI + "_" + type
}

export default apiMiddleware


// the api awaits for an "API" action, when it hits
// the api start fetching to the node app 
// API_START and API_END are used in every reducers therefore before a dispatch 
// to every action is added the reducers name for which is intended the action
// every reducers holds a LoadingData state the API_START and API_END thick on and off this state
// finally the reducers holds the data that is send back
// but they do not yet apprend a failure or a responce from the api