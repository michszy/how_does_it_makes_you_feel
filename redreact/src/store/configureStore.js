
import { createBrowserHistory } from 'history'
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import createRootReducer  from "../reducers/reducers"
import { routerMiddleware } from 'connected-react-router'
import apiMiddleware from "../middleware/api"

export const history = createBrowserHistory()



export default function configureStore(preloadedState) {
  const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(
    createRootReducer(history),
    preloadedState,
    composeEnhancer(applyMiddleware(
      routerMiddleware(history),
      apiMiddleware, 
      thunk,
      ),
    ),
  )

    if (module.hot) {
      // Enable Webpack hot module replacement for reducers
      module.hot.accept('../reducers/reducers', () => {
        store.replaceReducer(createRootReducer(history));
      });
  }
  
  return store
}

// configure store :
// - passing in middleware
// - passing in reducers
