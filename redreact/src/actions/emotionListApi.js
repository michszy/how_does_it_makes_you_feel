import { apiAction } from "./commonActions"

export const FETCH_EMOTIONS = "FETCH_EMOTIONS"
export const SET_EMOTIONS = "SET_EMOTIONS"
    
export const fetchEmotions = () => {
return apiAction({
    url : "http://localhost:5000/api/emotions/getAll",
    onSuccess : setEmotions,
    onFailure : () =>  console.log("Error occured loading emotions"),
    label : FETCH_EMOTIONS,
    type : "EMOTIONS"
    })
}

const setEmotions = (data) => {
    return {
        type : SET_EMOTIONS,
        payload : data
    }
}