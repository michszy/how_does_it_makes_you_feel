export const API = "API";
export const API_START = "API_START";
export const API_END = "API_END";
export const ACCESS_DENIED = "ACCESS_DENIED";
export const API_ERROR = "API_ERROR";

export const apiAction = ({
    url = "",
    method = "GET", 
    data = null,
    accessToken = null,
    onSuccess = () => {},
    onFailure = () => {},
    label = "",
    headerOverride = null,
    type = "NOTHING"
}) => {
    return {
        type : API,
        payload : {
            url,
            method,
            data,
            accessToken,
            onSuccess,
            onFailure,
            label,
            headerOverride,
            type
        }
    }
}



export const apiStart = (type, label) => ({
    type: type,
    payload: label
  });
  
export const apiEnd = (type, label) => ({
type: type,
payload: label
});

export const accessDenied = (type, url) => ({
type: type,
payload: {
    url
}
});

export const apiError = (type, error) => ({
type: type,
error
});
