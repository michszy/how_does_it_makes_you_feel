import { apiAction } from "./commonActions"


export const FETCH_USERINFO_LOGIN = "FETCH_USERINFO_LOGIN"
export const SET_USERINFO_LOGIN = "SET_USERINFO_LOGIN"

export const FETCH_USERINFO_REGISTER = "FETCH_USERINFO_REGISTER"
export const SET_USERINFO_REGISTER = "SET_USERINFO_REGISTER"

export const isLogin = () => {
    return {
        type : "IS_LOGIN"
    }
} 



export const handleLogin = (email, password) => {
    console.log(email);
    console.log(password);
    
    return apiAction({
        url : `http://localhost:5000/users/login`,
        onSuccess : makeLogin,
        onFailure : () => console.log("Error occured loading logins"),
        label : FETCH_USERINFO_LOGIN,
        type : "USERINFO"
        
    })
}

export const handleRegister = (name, email, password, confirmPassword) => {
    const data = {
        name: name,
        email: email,
        password: password,
        confirmPassword: confirmPassword
    }
    
    return apiAction({
        url : `http://localhost:5000/api/users/register`,
        method : "POST",
        data : data,
        onSuccess : makeRegister,
        onFailure : () => console.log("Error occured loading logins"),
        label : FETCH_USERINFO_REGISTER,
        type : "USERINFO"
        
    })
    
}

const makeLogin = (userInfo) => {
    return {
        type : SET_USERINFO_LOGIN,
        payload : userInfo
    }
}

const makeRegister = (userInfo) => {
    console.log(userInfo);
    
    return {
        type : SET_USERINFO_REGISTER,
        payload : userInfo
    }
}