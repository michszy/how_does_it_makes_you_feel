import React from "react"

import FirstQuestion from '../containers/FirstQuestion'
import SecondQuestion from '../containers/SecondQuestion'
import Answers from '../containers/Answers'
import Login from '../containers/Login'
import Register from '../containers/Register'

import { Route, Switch } from 'react-router-dom'

const routes = (
    <div>
        <Switch>
            <Route path="/" component={FirstQuestion} exact />
            <Route path="/reasons"component={SecondQuestion} />
            <Route path="/answers/:reason"component={Answers} />
            <Route path="/login"component={Login} />
            <Route path="/register" component={Register} />
        </Switch>
    </div>
)

export default routes

// holding the routes for the url path in the app which renders the Components/Containers