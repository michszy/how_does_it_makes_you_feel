import React, { Component } from 'react'
import { Button, Row , Grid, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import { handleLogin, isLogin } from "../actions/userInfoApi"
import { connect } from 'react-redux'

class Login extends Component {

    constructor(props) {
        super(props)    
        this.state = {
            email : "test@test.com",
            password : "test"
        }
        this.history = this.props.history
        this.error = {}
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.isUserLogged = this.state.isUserLogged
    }

    validateForm () {
        return this.state.email.length > 0 && this.state.password.length > 0
    }

    handleChange = event => {
        this.setState({
            [event.target.id] : event.target.value
        });
    }

    handleSubmit(event) {
        console.log("A name was submitted " + this.state.email)
        console.log("A password was submitted " + this.state.password)
        this.props.handleLogin(this.state.email, this.state.password)
        this.props.isLogin()
       
        event.preventDefault();

    }

    componentDidUpdate() {
        if (this.props.isUserLogged === true) {
            this.history.push("/")
        } 
    }

    
  
      render() {
        const error = (<div><h1>{this.state.error}</h1></div>)

            return (
                <div>
        <Grid>
            <Row className="justify-content-md-center ">
            <div className="Login">
                    <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="email" bsSize="small">
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                        autoFocus
                        type="email"
                        value={this.state.email}
                        onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="small">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                        value={this.state.password}
                        onChange={this.handleChange}
                        type="password"
                        />
                    </FormGroup>
            <Button
                block
                bsSize="sm"
                disabled={!this.validateForm()}
                type="submit"
            >
                Login
            </Button>
                    </form>
            </div>
            <div>
                {error}
            </div>
            </Row>
        </Grid>

        </div>
          )
      }
  }

  const mapStateToProps = state => ({
    data : state.userInfo.data,
    isLoadingData : state.userInfo.isLoadingData,
    isUserLogged : state.userInfo.isUserLogged
  });

  export default connect(mapStateToProps, {
    handleLogin, isLogin
  })(Login)

  // is Login successfull redirect to the previous page
  // if aint send an alert

  