import React, { Component } from 'react'
import { Button, Row , Grid, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import { handleRegister, isLogin } from "../actions/userInfoApi"
import { connect } from 'react-redux'

/*
 this.state = {
  isLoading: false,
  email: "",
  password: "",
  confirmPassword: "",
  confirmationCode: "",
  newUser: null
};
*/

class Register extends Component {

  constructor(props) {
    super(props)    
    this.state = {
        username : "Mich",
        email : "test@test.com",
        password : "test",
        confirmPassword: "test",
        newUser: null
    }
    this.history = this.props.history
    this.error = {}
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.isUserLogged = this.state.isUserLogged
}

validateForm () {
    let isValid
    if (this.state.email.length > 0 && this.state.password.length > 0) {
      isValid = true
    } else if (this.state.password === this.state.confirmPassword) {
      isValid = true
    } else {
      isValid = false
    }
    return isValid
}

handleChange = event => {
    this.setState({
        [event.target.id] : event.target.value
    });
}

handleSubmit(event) {
    this.props.handleRegister(this.state.username, this.state.email, this.state.password, this.state.confirmPassword)
    this.props.isLogin()
   
    event.preventDefault();

}

componentDidUpdate() {
    if (this.props.isUserLogged === true) {
        //this.history.push("/")
    } 
}
 
  
    render() {
      const error = (<div><h1>{this.state.error}</h1></div>)
          return (
            <div>
        <Grid>
            <Row className="justify-content-md-center ">
            <div className="Login">
                    <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="email" bsSize="small">
                        <ControlLabel>Name</ControlLabel>
                        <FormControl
                        autoFocus
                        type="text"
                        value={this.state.username}
                        onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="email" bsSize="small">
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                        autoFocus
                        type="email"
                        value={this.state.email}
                        onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="small">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                        value={this.state.password}
                        onChange={this.handleChange}
                        type="password"
                        />
                    </FormGroup>
                    <FormGroup controlId="email" bsSize="small">
                        <ControlLabel>Confirm Password</ControlLabel>
                        <FormControl
                        autoFocus
                        type="password"
                        value={this.state.confirmPassword}
                        onChange={this.handleChange}
                        />
                    </FormGroup>
            <Button
                block
                bsSize="sm"
                disabled={!this.validateForm()}
                type="submit"
            >
                Register
            </Button>
                    </form>
            </div>
            <div>
                {error}
            </div>
            </Row>
        </Grid>

        </div>
          )
      }
  }
  const mapStateToProps = state => ({
    data : state.userInfo.data,
    isLoadingData : state.userInfo.isLoadingData,
    isUserLogged : state.userInfo.isUserLogged
  });

  export default connect(mapStateToProps, {
    handleRegister, isLogin
  })(Register)