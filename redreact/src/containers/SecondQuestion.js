import React, { Component } from 'react'
import { connect } from 'react-redux'
import NavBarHeader from '../containers/NavBarHeader';
import 'bootstrap/dist/css/bootstrap.min.css';

class SecondQuestion extends Component {

  constructor(props) {
    super(props)
    this.history = this.props.history
  }


    render() {
    const history = this.props.history
    const data = this.props.data

    const style = {
      margin : "40px"
    }
     
      let mapp;
      if (data !== undefined) { 
        mapp = data.map(emotion => {
         return <div key={emotion["_id"]}><p >{emotion["name"]}</p><br/></div>
        })
      } else {
        return (
          <div>
          <NavBarHeader history= {history}/>
        <div className="container">
        <div className="row">
        <div className="col-12 d-flex justify-content-center">
        <h2><i>Why ?</i> </h2>
          </div>
        </div>
        <div className="row">
            <div className="col-12 d-flex justify-content-center">
            <button type="button" className="btn btn-outline-primary btn-lg" style={style} onClick={() => { this.history.push("/answers/sadness/id_reas_01")}}>I feel like I don't have a reason to live</button>
            </div>
          </div>
          <div className="row">
            <div className="col-12 d-flex justify-content-center">
            <button type="button" className="btn btn-outline-success btn-lg" style={style}>I'm having suicidal thoughts</button>
            <button type="button" className="btn btn-outline-danger btn-lg" style={style}>My future seems so incertain</button>
            </div>
          </div>
          <div className="row">
            <div className="col-12 d-flex justify-content-center">
            <button type="button" className="btn btn-outline-warning btn-lg" style={style}>I have bad relationship with my family</button>
            <h2 style={style}>Sadness</h2>
            <button type="button" className="btn btn-outline-dark btn-lg" style={style}>I don't know know if my friends like me</button>
            </div>
          </div>
          <div className="row">
            <div className="col-12 d-flex justify-content-center">
            <button type="button" className="btn btn-outline-danger btn-lg" style={style}>Everythings bores me</button>
            <button type="button" className="btn btn-outline-info btn-lg" style={style}>I feel sad but for no reason</button>
            </div>
          </div>
          <div className="row">
            <div className="col-12 d-flex justify-content-center">
            <button type="button" className="btn btn-outline-dark btn-lg" style={style}>I only find hapiness in drugs or alcool</button>
            </div>
            </div>
          </div>
          
          </div>
        )
      }
      
      return (
      <div>
          
          <div>
            {mapp}
          </div>
      </div>

      )
    }
}

const mapStateToProps = state => ({
  data : state.emotionList.data,
  isLoadingData : state.emotionList.isLoadingData
});


export default connect(mapStateToProps, {
  
})(SecondQuestion)

/*
<div class="jumbotron jumbotron-fluid">
            <div class="container">
              <p class="lead">On this second page, the user following the answer of the first question is proposed to more concrete and specific reasons for why he or she is in that state.</p>
              <p class="lead">Once again, the users maybe be getting a moderation role will have the possibily to add up reason that weren't previously add up. Each of these reasons falls from the previously answered question. </p>
              <p class="lead">Before we continue allow me to enumerate the technology that run this app. I used ReactJS and Redux for the front end, NodeJS for the backend, MongoDB for the database and finnaly I'm develloping on Linux/Ubuntu. </p>
              <p class="lead">I invite you to click the "I feel like I don't have a reason to live" button on the top to go to the third and last panel.</p>
            </div>
          </div>
*/