import React, { Component } from 'react'
import { Navbar, Nav, NavItem} from 'react-bootstrap';
import { connect } from 'react-redux'
import 'bootstrap/dist/css/bootstrap.min.css';

class NavBarHeader extends Component  {
  constructor(props){
    super(props)
    this.history = this.props.history
    this.isUserLogged = this.props.isUserLogged
  }


  render() {

    const spanStyle = {
      display:"inline-block",
      width: "20%"
    }

    let UserInfoNav
    if (this.isUserLogged === true) {
      UserInfoNav = (
        <button className="nav-link" >Profile</button>
      )
    } else {
      UserInfoNav = (
      <div className="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul className="navbar-nav ml-auto">
            <li className="nav-item active" >
              <button className="btn btn-light" onClick={() => { this.history.push("/login")}}>Login</button>
            </li>
            <span style={spanStyle}></span>
            <li className="nav-item active" >
            <button className="btn btn-light" onClick={() => { this.history.push("/register")}}>Register</button>>
            </li>
        </ul>
        </div>

      )
    }
      
  return (
    
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <button className="btn btn-light" onClick={() => { this.history.push("/")}}>Home </button>
        
        {UserInfoNav}

    </nav>
    
    )
  }

}


const mapStateToProps = state => ({
    isUserLogged : state.userInfo.isUserLogged
})

const mapDispatchToProps = dispatch => {
  return {
    logIn : () => {dispatch({type:"LOG_IN"})},
    logOut : () => {dispatch({type:"LOG_OUT"})}
}
}


export default connect(mapStateToProps, mapDispatchToProps)(NavBarHeader)

// map state to props => Login => props is Login
// Containers => get the props pass it to the child and the components decide if it is materials for a connect materials or access profile materials


// history props serve for the circulation within the app to not lose the states from the reducers