import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.js';
import "../components/styleSheets/style.css"

import NavBarHeader from '../containers/NavBarHeader';

export default class Answers extends Component {
    constructor(props) {
        super(props)
        this.history = this.props.history
      }
    render() {
        const history = this.props.history
        return (
        <div>
          <NavBarHeader history= {history}/>
          <div className="maiDiv">
            <div class="container">
                <div class="row" >
                    <div class="col-3">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active " id="v-pills-posts-tab" data-toggle="pill" href="#v-pills-posts" role="tab" aria-controls="v-pills-posts" aria-selected="true">Posts</a>
                        <a class="nav-link" id="v-pills-articles-tab" data-toggle="pill" href="#v-pills-articles" role="tab" aria-controls="v-pills-articles" aria-selected="false">Articles</a>
                        <a class="nav-link" id="v-pills-video-tab" data-toggle="pill" href="#v-pills-video" role="tab" aria-controls="v-pills-video" aria-selected="false">Video</a>
                        <a class="nav-link" id="v-pills-memes-tab" data-toggle="pill" href="#v-pills-memes" role="tab" aria-controls="v-pills-memes" aria-selected="false">Memes/GIFs</a>
                        <a class="nav-link" id="v-pills-podcasts-tab" data-toggle="pill" href="#v-pills-podcasts" role="tab" aria-controls="v-pills-podcasts" aria-selected="false">Podcasts</a>
                        <a class="nav-link" id="v-pills-music-tab" data-toggle="pill" href="#v-pills-music" role="tab" aria-controls="v-pills-music" aria-selected="false">Music</a>
                        <a class="nav-link" id="v-pills-books-tab" data-toggle="pill" href="#v-pills-books" role="tab" aria-controls="v-pills-books" aria-selected="false">Books</a>
                        <a class="nav-link" id="v-pills-merch-tab" data-toggle="pill" href="#v-pills-merch" role="tab" aria-controls="v-pills-merch" aria-selected="false">Merch</a>
                    </div>
                    </div>
                    <div class="col-1"></div>
                    <div class="col-8">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-posts" role="tabpanel" aria-labelledby="v-pills-posts-tab">
                            <div>
                                <h4>Definitely been there too my friend. What has worked really well for me is writing about what the healthier alternative to drinking/drugging would be and then remembering to read what I wrote over and over again until I can start making it happen. Therapy was also very important for me because I was using alcohol and Netflix as an escape from responsibility and I wouldn’t have learned that unless I had help from others who could see my behaviors.</h4>
                                <h4>I’m in the same boat. I’ll keep watching for someone to comment a cure...but I’m thirty and have been depressed since I was probably about 9 or 10. Two unsuccessful attempts one when I was in my young teens and one fairly recently, my family knows about both but still when I say I need help or just a shoulder I have to come to a reddit page and cry to strangers who probably aren’t actually as concerned as they say.</h4>
                                <h4>But what exactly do you think makes it fade away? I also have dissociation disorder but I feel like the more I let it develop, the more it irreversibily becomes my true self. After my first couple of years with depression, I started casting this way of life, identifying every unfulfilled need that came after losing contact with people around me, experimenting with self-dependent ways to replace them until I could actually go through large periods of time with no human contact whatsoever. But the sorrow at the moments of realisation and the anxiety from losing my social senses grew larger. At one point I decided to make every choice, as hard as it felt, just to snap out of it. Another 3-4 years spent working to get out of emotional isolation and live like a normal human being, with zero accomplishments except professionally and then I reached my limit again. At that point my brain reverted back to the dissociation state, trying to save me from a tragic outcome. I just don't know if I should oppose it any more, since it might be the healthiest option considering the alternatives.</h4>
                                <br/>
                                <p>Source : Reddit r/depression</p>
                                <div className="right">
                                <button class="btn btn-success btn-lg"> Another One </button>
                                <br/>
                                <br/>
                                <button  type="button" className="btn btn-info btn-lg">Post One</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-articles" role="tabpanel" aria-labelledby="v-pills-articles-tab">
                            <div>
                            <iframe width="100%" height="500px" src="https://psychologia.co/why-am-i-so-sad/" allow="fullscreen"></iframe>
                            <br/>
                            <div className="right">
                            <button class="btn btn-success btn-lg"> Another One </button>
                            <br/>
                                <br/>
                                <button  type="button" className="btn btn-info btn-lg">Post One</button>
                            </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-video" role="tabpanel" aria-labelledby="v-pills-video-tab">
                        <div>
                            <iframe width="100%" height="500px" src="https://www.youtube.com/embed/gJvehKYb8mA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <br/>
                            <div className="right">
                            <button class="btn btn-success btn-lg"> Another One </button>
                            <br/>
                                <br/>
                                <button  type="button" className="btn btn-info btn-lg">Post One</button>
                                </div>
                        </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-memes" role="tabpanel" aria-labelledby="v-pills-memes-tab">
                            <img witdh="500px" height="500px" src="https://meme.xyz/uploads/posts/t/l-21642-sadness.jpg"></img>
                            <br/>
                            <div className="right">
                            <button class="btn btn-success btn-lg"> Another One </button>
                            <br/>
                                <br/>
                                <button  type="button" className="btn btn-info btn-lg">Post One</button>
                                </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-podcasts" role="tabpanel" aria-labelledby="v-pills-podcasts-tab">
                            <iframe width="100%" height="500px"  src="https://www.youtube.com/embed/-SJywvgaJEI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <br/>
                            <div className="right">
                            <button class="btn btn-success btn-lg"> Another One</button>
                            <br/>
                                <br/>
                                <button  type="button" className="btn btn-info btn-lg">Post One</button>
                                </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-music" role="tabpanel" aria-labelledby="v-pills-music-tab">
                        <iframe width="100%" height="500px" src="https://www.youtube.com/embed/amwQytRNvEw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <div className="right">
                            <button class="btn btn-success btn-lg"> Another One  </button>
                            <br/>
                                <br/>
                                <button  type="button" className="btn btn-info btn-lg">Post One</button>
                                </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-books" role="tabpanel" aria-labelledby="v-pills-books-tab">
                            <iframe width="100%" height="500px" src="https://www.senscritique.com/livre/Les_Souffrances_du_jeune_Werther/105929"></iframe>
                            <div className="right">
                            <button class="btn btn-success btn-lg"> Another One </button>
                            <br/>
                                <br/>
                                <button  type="button" className="btn btn-info btn-lg">Post One</button>
                                </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-merch" role="tabpanel" aria-labelledby="v-pills-merch-tab">
                            <img witdh="500px" height="500px" src="https://images-na.ssl-images-amazon.com/images/I/81GCVifVs%2BL._SY550_.jpg"></img>
                            <button class="btn btn-warning btn-lg"> Where to buy </button>
                            <div className="right">
                            

                            <br/>
                            <br/>
                            <button class="btn btn-success btn-lg"> Another One </button>
                            <br/>
                            
                            
                                <br/>
                                <button  type="button" className="btn btn-info btn-lg">Post One</button>
                                </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        
        </div>        
        )
    }
}

/*
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <p class="lead">On this last page, the user is given a selection of content to heal his or her sorrow</p>
        <p class="lead">The users can as well propose new content or write their own post about their ill and might as well discuss it in a future comment section</p>
        <p class="lead">Because the depression state is a very sensitive matter, all the contents will have to be reviewed and validated and moderated since I'm also planning and working on a login and register profile feature (Google OAuth, Facebook Login and Email) </p>
        <p class="lead">The content will be aswell upvoted or downvotes and shared. Also as you can see you can by clicking on the "Another one" button the user will have the possibity to circle thru the other content. The merch panel can be an occasion to make publicity for product judged usefull in this particular reason of sadness, so to gain some profit. Also all the content displayed here are setup only for the use of an example.</p>
        <p class="lead">While beeing a Junior DataScientist, the data gathered by this site will also maybe allow a machine learning project to bloom. All the proff of concept had been made it will be in short terms that I will finish what I begun</p>
        <p class="lead">That's it, thank you for taking up the time to discover my project</p>
    </div>
</div>
*/