import React, { Component } from 'react'
import { connect } from 'react-redux'
import {fetchEmotions} from "../actions/emotionListApi"
import NavBarHeader from '../containers/NavBarHeader';
import 'bootstrap/dist/css/bootstrap.min.css';

class FirstQuestion extends Component {

  constructor(props) {
    super(props)
    this.history = this.props.history
  }

  componentWillMount() {
    if (this.props.data == null) {
    this.props.fetchEmotions()
    }
  }
    render() {
    const history = this.props.history
    const data = this.props.data

    const style = {
      margin : "40px"
    }

    let mapp = (
          <div>
          <NavBarHeader history= {history}/>
        <div className="container">
        <div className="row">
        <div className="col-12 d-flex justify-content-center">
        <h1> <i>How do you feel ? </i></h1>
          </div>
        </div>
          <div className="row">
            <div className="col-12 d-flex justify-content-center">
            <button type="button" className="btn btn-primary btn-lg" onClick={() => { this.history.push("/reasons")}} style={style}>Sadness</button>
            </div>
          </div>
          <div className="row">
            <div className="col-12 d-flex justify-content-center">
            <button type="button" className="btn btn-success btn-lg" style={style}>Anger</button>
            <button type="button" className="btn btn-danger btn-lg" style={style}>Heartbreak</button>
            </div>
          </div>
          <div className="row">
            <div className="col-12 d-flex justify-content-center">
            <button type="button" className="btn btn-warning btn-lg" style={style}>Sorrow</button>
            <button type="button" className="btn btn-info btn-lg" style={style}>Solitude</button>
            <button type="button" className="btn btn-dark btn-lg" style={style}>Depression</button>
            </div>
          </div>
          <div className="row">
            <div className="col-12 d-flex justify-content-center">
            <button type="button" className="btn btn-danger btn-lg" style={style}>Jealous</button>
            <button type="button" className="btn btn-info btn-lg" style={style}>Feeling Lost</button>
            </div>
          </div>
          <div className="row">
            <div className="col-12 d-flex justify-content-center">
            <button type="button" className="btn btn-dark btn-lg" style={style}>Grief</button>
            </div>
            </div>
          </div>
          </div>
        
        )
      
      
      return (
      <div>
          <div>
            {mapp}
          </div>
      </div>

      )
    }
}

const mapStateToProps = state => ({
  data : state.emotionList.data,
  isLoadingData : state.emotionList.isLoadingData
});


export default connect(mapStateToProps, {
  fetchEmotions
})(FirstQuestion)


/*          
<div class="jumbotron jumbotron-fluid">
            <div class="container">
              <h5 class="display-4">Welcome to How Does It Makes You Feel </h5>
              <a href="https://www.linkedin.com/in/michel-szychiewicz-900630138/"><p>Here is the link to my LinkedIn profile to authenticate that it is I</p></a>
              <p class="lead">That litle text is only that you read on the bottom is display only to explain what is the essence of my project. It will be of course remove in the production version and in fact nothing works, the backend isn't setup here and even the front has made in a hury and I plan for it to be a lot prettier.</p>
              <p class="lead"> While waiting for the backend to be finished and he is almost ready, allow me to introduce you to my app</p>
              <p class="lead">HDIMYF is an app which try to give some help to people who feel sad. While there are different way of felling bad, on this first page, the user click on the type of sadness which he or she feels.</p>
              <p class="lead">While there are 9 buttons displayed here, my app souls rest on users who add up in this first case ways on feeling bad and in the following cases, reasons and articles. Yet in this first version, showing only a slice of the UX, only the sadness button on the top is working so I invite to click on it  </p>
            </div>
</div>
*/