import {
    SET_USERINFO_LOGIN,
    FETCH_USERINFO_LOGIN,
    SET_USERINFO_REGISTER
} from "../actions/userInfoApi"


const userInfo = (state ={isUserLogged : false}, action) => {
    
    console.log("action type ---> " ,action.type)
    switch (action.type) {
        case SET_USERINFO_LOGIN:
           return {
               ...state, 
               data : action.payload,
            }

        case SET_USERINFO_REGISTER:
            return {
                ...state,
                newUser : true
            }
            
                
        case "API_START_USERINFO":
            if (action.payload === FETCH_USERINFO_LOGIN) {
                return {
                    ...state,
                    LoadingData : true
                }
            }
            break;
        case "API_END_USERINFO":
            if (action.payload === FETCH_USERINFO_LOGIN) {
                return {
                    ...state,
                    LoadingData : false
                }
            }
            break;
        case "IS_LOGIN" :
            return {
                isUserLogged : true
            }
        default:
            return state
    }
}


export default userInfo 