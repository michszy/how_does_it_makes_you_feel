import {
    SET_EMOTIONS,
    FETCH_EMOTIONS
} from "../actions/emotionListApi"


const emotionList = (state ={}, action) => {
    
    console.log("action type ---> " ,action.type)
    switch (action.type) {
        case SET_EMOTIONS:
        console.log("___ " + action.payload);
        
           return {
               data : action.payload
            }
                
        case "API_START_EMOTIONS":
            if (action.payload === FETCH_EMOTIONS) {
                return {
                    ...state,
                    isLoadingData : true
                }
            }
            break;
        case "API_END_EMOTIONS":
            if (action.payload === FETCH_EMOTIONS) {
                return {
                    ...state,
                    isLoadingData : false
                }
            }
            break;
        default:
            return state
    }
}


export default emotionList 