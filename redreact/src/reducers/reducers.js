import emotionList from "./emotionList"
import userInfo from "./userInfo"

import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

const rootReducer =  (history) => combineReducers({
    userInfo : userInfo,
    emotionList : emotionList,
    router : connectRouter(history)
})

export default rootReducer

// combining and exporting the reducers to the store 
// router is an key that must to keep and is part of the connected-react-router librairie