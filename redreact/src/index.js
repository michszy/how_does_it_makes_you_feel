import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';

import { AppContainer } from 'react-hot-loader'
import { Provider } from 'react-redux'
import App from './containers/App';
import configureStore, { history } from './store/configureStore';


const store = configureStore()

const render = () => {
    ReactDOM.render(
        <AppContainer>
            <Provider store={store}>
                <App history={history} />
            </Provider>
        </AppContainer>,
        document.getElementById('root')
    )
}


render()

// Hot reloading
if (module.hot) {
    // Reload components
    module.hot.accept("./containers/App", () => {
      render()
    })
}

// first link/step to the rendering
// passing in the store and the history
// App in cointaing the routes


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

