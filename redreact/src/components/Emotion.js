import React, { Component } from 'react'
import {Button, Col} from "react-bootstrap";
import {Link} from 'react-router-dom';

export default class Emotion extends Component {

  

  render() {
    return (
      <Col xs={this.props.col} md = {this.props.col}>
          <Link to={{pathname: '/reasons', search: `?emotion=${this.props.emotion}`}}><Button> {this.props.emotion}</Button></Link>
      </Col>
    )
  }
}
