const express = require("express")
const passport = require("passport")
const morgan = require("morgan");
const mysql = require("mysql");
const cookieParser = require("cookie-parser")
var cors = require('cors');
var bodyParser = require('body-parser');
var session = require("express-session")

const PORT = process.env.PORT || 5000;

var app = express()
//app.use(bodyParser.urlencoded({extended : false}));
app.use(bodyParser.json())



//enables cors
app.use(function(req, res, next) { 
    res.header("Access-Control-Allow-Origin", "http://localhost:5000"); 
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"); 
    next(); 
}); 

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Headers', 'content-type');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});


app.use(session({
    secret : "secret",
    resave : true,
    saveUninitialized : true
}))

app.use(bodyParser.urlencoded({extended : true}))
app.use(bodyParser.json())


app.get("/register", (req, res) => {
    var email =req.query.email
    var password = req.query.password
    var userName = req.query.userName

    // look for google and facebook login
    // if user is not already register
    // insert into



    var now = new Date().getTime()

})


app.get("/auth", (req, res) => {
    console.log("call to auth");
    var email = req.query.email
    var password = req.query.password
    console.log(email);
    console.log(password);
    
    if (email && password) {
        connection.query("SELECT * FROM user WHERE email = ? AND user_password = ?" ,
        [email, password], (error, results, fields) => {
            console.log(results);
            
            if (results.length > 0 ) {
                req.session.loggedin = true
                req.session.email = email

                res.send(results[0])

            } else {
                res.send("Incorrect Email and/ or Password")
            }
            res.end()
        })
    } else {
        res.send("Please enter Email and Password")
        res.end()
    }

})

app.get("/emotion/all", (req, res) => {
    console.log("select all");
    
    QUERY_SELECT_ALL = "SELECT * FROM emotion"
    connection.query(QUERY_SELECT_ALL, (error, results) => {
        if (error) {
            return res.send(err)
        } else {
            return res.json({
                results : results,
            })
        }
    });
});

app.get("/emotion", (req, res) => {
    emotion = parseInt(req.query.emotion)
    
    console.log(emotion);
    
        QUERY_SELECT_SOME = `SELECT * FROM emotion WHERE id_emotion = ${emotion};`
    console.log(QUERY_SELECT_SOME);
    
    connection.query(QUERY_SELECT_SOME, (error, results) => {
        if (error) {
            return res.send(err)
        } else {
            return res.json({
                results : results
            })
        }
    });
});






app.listen(PORT, () => {
    console.log("server is up and listenning on port " + PORT);
 
})