const express = require('express');
const router = express.Router();

const Emotion = require('../models/Emotions');


router.get('/getAll', (req, res) => {

    Emotion.find({}, (err, emotion) => {

        res.send(emotion)
    })

})

module.exports = router