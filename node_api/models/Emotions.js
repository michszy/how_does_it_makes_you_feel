const mongoose = require("mongoose")

const Schema = mongoose.Schema;

const EmotionSchema = new Schema({
    name : {
        type: String,
        required: true
    },
    importance : {
        type: Number
    },
    created_by: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    }
})

const Emotion = mongoose.model("emotion", EmotionSchema)

module.exports = Emotion