const mongoose = require("mongoose")

const Schema = mongoose.Schema;

const IssuesSchema = new Schema({
    name : {
        type: String,
        required: true
    },
    importance : {
        type: Number
    },
    created_by: {
        type: String
    },
    emotion: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    }
})

const Issue = mongoose.model("issues", IssuesSchema)

module.exports = Issue