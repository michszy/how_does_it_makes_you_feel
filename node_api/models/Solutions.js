const mongoose = require("mongoose")

const Schema = mongoose.Schema;

const SolutionSchema = new Schema({
    type : {
        type : String,
        required: true
    },
    title : {
        type: String,
        required: true
    },
    content : {
        type: String
    },
    created_by: {
        type: String
    },
    points: {
        type: String
    },
    views: {
        type: Number
    },    
    url: {
        type: String
    },
    date: {
        type: Date,
        default: Date.now
    }
})

const Solution = mongoose.model("solutions", SolutionSchema)

module.exports = Solution