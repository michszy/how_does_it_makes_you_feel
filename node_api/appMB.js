const express = require("express")
const mongoose = require("mongoose")
const bodyParser = require("body-parser")
const passport = require("passport")
const config = require("./db")

const users = require('./routes/user'); 
const emotions = require("./routes/emotion")

mongoose.connect(config.DB, { useNewUrlParser : true}).then(
    () => {console.log("Database is connected")}, 
    err => {console.log("Error while connecting to database " + err);}
)

const app = express()


app.use(function(req, res, next) { 
    res.header("Access-Control-Allow-Origin", "http://localhost:3000"); 
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"); 
    next(); 
}); 

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Headers', 'content-type');
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use(passport.initialize())
require("./passport")(passport)

app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json())

app.use("/api/users", users)

app.use("/api/emotions", emotions)

app.get("/", (req, res) => {
    res.send("'sup")
})

app.get("/auth", (req, res) => {
    console.log(req.query.password);
    console.log(req.query.email);
    
    /* 
    the password must be serialized
    mongo 
    */
})


const PORT = process.env.PORT || 5000

app.listen(PORT, () => {
    console.log(`Server is running on PORT ${PORT}`);
})